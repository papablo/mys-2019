FROM python

WORKDIR /usr/src/

COPY requirements.txt .

RUN pip install -r requirements.txt && jupyter nbextension enable --py widgetsnbextension

EXPOSE 10000
