from simulacion.simulate import simulate
from pprint import pprint


def main():
    params = {
        "MAX_MESES": 3,
        "MAX_DIAS": 30,
        "MAX_DIAS_PRODUCCION": 10,
        "MAX_OPERARIOS": 2,
        "CANT_MINUTOS_JORNADA": 60*8,
        "M4": {
            "CANT_PRODUCCION_DIA": 30,
            "MEDIA_ATENCION_PEDIDO": 12,
            "MEDIA_GENERACION_PEDIDOS": 30,
            "DESVIO_GENERACION_PEDIDOS": 5
        },
        "M6": {
            "CANT_PRODUCCION_DIA": 20,
            "MEDIA_ATENCION_PEDIDO": 18,
            "MEDIA_GENERACION_PEDIDOS": 25,
            "DESVIO_GENERACION_PEDIDOS": 3
        },
        "M8": {
            "CANT_PRODUCCION_DIA": 10,
            "MEDIA_ATENCION_PEDIDO": 23,
            "MEDIA_GENERACION_PEDIDOS": 18,
            "DESVIO_GENERACION_PEDIDOS": 2
        },
    }


main()
