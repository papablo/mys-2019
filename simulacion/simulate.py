from simulacion.Estado import Estado
from time import sleep


def simulate(params):
    """
    Realiza la simulación
    Recibe:
        params = {
            MAX_MESES,
            MAX_DIAS,
            MAX_DIAS_PRODUCCION,
            MAX_OPERARIOS,
            CANT_MINUTOS_JORNADA,
            [M4|M6|M8]: {
                CANT_PRODUCCION_DIA.
                MEDIA_ATENCION_PEDIDO,
                MEDIA_GENERACION_PEDIDOS,
                DESVIO_GENERACION_PEDIDOS
            }
        }
    Devuelve:
        (
            kits_fabricados_por_anio: cantidad de kits de piezas fabricados en el año simulado,
            cant_pedidos_resueltos_por_anio: cantidad de pedidos resueltos en el año simulado,
            cant_pedidos_sin_resolver_por_anio: cantidad de pedidos sin resolver en el año simulado
            pedidos_resueltos_por_mes: lista con las cantidades de pedidos resueltos en cada mes,
            pedidos_sin_resolver_por_mes: lista con las cantidades de pedidos sin resolver en cada mes,
            pedidos_resueltos_por_dia: lista con los pedidos resueltos cada día,
            pedidos_sin_resolver_por_dia: lista con los pedidos sin resolver cada día,
            disponibilidad: diccionario con los distintos niveles de ocupación de los operarios (teniendo dos operarios, los niveles serían 0, 1, 2), y la cantidad de minutos de cada nivel
        )
    """
    estado = Estado(params)

    kits_fabricados_por_anio = 0

    cant_pedidos_resueltos_por_anio = 0
    cant_pedidos_sin_resolver_por_anio = 0

    pedidos_resueltos_por_mes = []
    pedidos_sin_resolver_por_mes = []

    pedidos_resueltos_por_dia = []
    pedidos_sin_resolver_por_dia = []

    disponibilidad = []

    for mes in range(params["MAX_MESES"]):
        pedidos_resueltos_en_un_mes = []
        pedidos_sin_resolver_en_un_mes = []
        for dia in range(params["MAX_DIAS"]):

            # print(f"params['MAX_OPERARIOS'] = {params['MAX_OPERARIOS']}")
            estado.operarios_disponibles = params["MAX_OPERARIOS"]
            disponibilidad_por_dia = {}
            cant_pedidos_resueltos_en_un_dia = 0

            # Producir stock si es día de producción
            if dia < params["MAX_DIAS_PRODUCCION"]:
                stock_m4, stock_m6, stock_m8 = estado.producir_stock(params)
                kits_fabricados_por_anio += (stock_m4 + stock_m6 + stock_m8)

            # Generar pedidos aleatoriamente
            cant_pedidos_generados_en_un_dia, cant_total_pedidos = estado.generar_pedidos(
                params)

            for minuto in range(params["CANT_MINUTOS_JORNADA"]):
                # Resolver los eventos que se puedan en este minuto
                eventos = estado.get_eventos_a_resolver(minuto, params)
                for evento in eventos:
                    evento.resolverse(estado, params)

                # Cumplir los pedidos que se puedan con el stock y los operarios disponibles
                cant_pedidos_resueltos, cant_pedidos_sin_resolver = estado.cumplir_pedidos(
                    minuto, params)

                cant_pedidos_resueltos_en_un_dia += cant_pedidos_resueltos

                operarios_disponibles = estado.operarios_disponibles
                if operarios_disponibles in disponibilidad_por_dia:
                    disponibilidad_por_dia[operarios_disponibles] += 1
                else:
                    disponibilidad_por_dia[operarios_disponibles] = 0

            pedidos_resueltos_por_dia.append(
                cant_pedidos_resueltos_en_un_dia)
            cant_pedidos_sin_resolver_en_un_dia = (cant_pedidos_generados_en_un_dia - cant_pedidos_resueltos_en_un_dia) if (
                cant_pedidos_generados_en_un_dia - cant_pedidos_resueltos_en_un_dia) > 0 else 0
            pedidos_sin_resolver_por_dia.append(
                cant_pedidos_sin_resolver_en_un_dia)

            pedidos_resueltos_en_un_mes.append(
                cant_pedidos_resueltos_en_un_dia)
            pedidos_sin_resolver_en_un_mes.append(
                cant_pedidos_sin_resolver_en_un_dia)

            disponibilidad.append(disponibilidad_por_dia)

        pedidos_resueltos_por_mes.append(sum(pedidos_resueltos_en_un_mes))
        pedidos_sin_resolver_por_mes.append(
            sum(pedidos_sin_resolver_en_un_mes))
        print(f"Fin mes {mes}")

    cant_pedidos_resueltos_por_anio = sum(pedidos_resueltos_por_mes)
    cant_pedidos_sin_resolver_por_anio = sum(pedidos_sin_resolver_por_mes)

    return (kits_fabricados_por_anio,
            cant_pedidos_resueltos_por_anio,
            cant_pedidos_sin_resolver_por_anio,
            pedidos_resueltos_por_mes,
            pedidos_sin_resolver_por_mes,
            pedidos_resueltos_por_dia,
            pedidos_sin_resolver_por_dia,
            disponibilidad)
