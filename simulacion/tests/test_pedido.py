import unittest
from unittest.mock import patch
from simulacion.Pedido import Pedido
from simulacion.Estado import Estado
from simulacion.PedidoProcesadoEvent import PedidoProcesadoEvent


class TestPedido(unittest.TestCase):

    def setUp(self):
        self.pedido = Pedido("M4")
        self.params = {
            "MAX_OPERARIOS": 2,
            "M4": {
                "CANT_PRODUCCION_DIA": 10,
                "MEDIA_ATENCION_PEDIDO": 2
            },
            "M6": {
                "CANT_PRODUCCION_DIA": 10
            },
            "M8": {
                "CANT_PRODUCCION_DIA": 10
            },
        }
        return super().setUp()

    def tearDown(self):
        return super().tearDown()

    def test_indica_correctamente_si_se_puede_cumplir(self):
        estado = Estado(self.params)
        estado.producir_stock(self.params)
        self.assertTrue(self.pedido.puede_cumplirse(estado))

    def test_indica_correctamente_si_no_se_puede_resolver_por_falta_de_operarios(self):
        self.params["MAX_OPERARIOS"] = 0
        estado = Estado(self.params)
        estado.producir_stock(self.params)
        self.assertFalse(self.pedido.puede_cumplirse(estado))

    def test_indica_correctamente_si_no_se_puede_resolver_por_falta_de_stock(self):
        self.params["M4"]["CANT_PRODUCCION_DIA"] = 0
        estado = Estado(self.params)
        estado.producir_stock(self.params)
        self.assertFalse(self.pedido.puede_cumplirse(estado))

    def test_se_cumple_correctamente(self):
        estado = Estado(self.params)
        estado.producir_stock(self.params)
        stock_m4 = estado.stock["M4"]
        operarios_disponibles = estado.operarios_disponibles

        self.pedido.cumplirse(estado, 0, self.params)

        self.assertEqual(estado.stock["M4"], stock_m4 - 1)
        self.assertEqual(estado.operarios_disponibles,
                         operarios_disponibles - 1)

    @patch("numpy.random.exponential")
    def test_pedido_devuelve_el_evento_correcto_despues_de_cumplirse(self, mockExponential):
        mockExponential.return_value = 123
        min = 1

        estado = Estado(self.params)
        estado.producir_stock(self.params)

        evento = self.pedido.cumplirse(estado, min, self.params)

        self.assertIsInstance(evento, PedidoProcesadoEvent)
        self.assertEqual(evento.tiempo_inicial, min)
        self.assertEqual(evento.demora, mockExponential.return_value)

        mockExponential.assert_called_once_with(self.params["M4"]["MEDIA_ATENCION_PEDIDO"])
