import unittest
from unittest.mock import patch
from simulacion.Estado import Estado
from simulacion.Pedido import Pedido
from simulacion.PedidoProcesadoEvent import PedidoProcesadoEvent


class TestEstado(unittest.TestCase):
    def setUp(self):
        self.params = {
            "CANT_MINUTOS_JORNADA": 480,
            "MAX_OPERARIOS": 2,
            "M4": {
                "CANT_PRODUCCION_DIA": 45,
                "MEDIA_ATENCION_PEDIDO": 1,
                "MEDIA_GENERACION_PEDIDOS": 30,
                "DESVIO_GENERACION_PEDIDOS": 5
            },
            "M6": {
                "CANT_PRODUCCION_DIA": 12,
                "MEDIA_ATENCION_PEDIDO": 1,
                "MEDIA_GENERACION_PEDIDOS": 30,
                "DESVIO_GENERACION_PEDIDOS": 5
            },
            "M8": {
                "CANT_PRODUCCION_DIA": 20,
                "MEDIA_ATENCION_PEDIDO": 1,
                "MEDIA_GENERACION_PEDIDOS": 30,
                "DESVIO_GENERACION_PEDIDOS": 5
            },
        }
        self.estado = Estado(self.params)

    def tearDown(self):
        pass

    def test_produce_stock_correctamente(self):
        stock_m4, stock_m6, stock_m8 = self.estado.producir_stock(self.params)
        self.assertEqual(self.estado.stock["M4"], 45)
        self.assertEqual(self.estado.stock["M6"], 12)
        self.assertEqual(self.estado.stock["M8"], 20)
        self.assertEqual(stock_m4, self.estado.stock["M4"])
        self.assertEqual(stock_m6, self.estado.stock["M6"])
        self.assertEqual(stock_m8, self.estado.stock["M8"])

    def test_acumula_stock_correctamente(self):
        self.estado.producir_stock(self.params)
        self.estado.producir_stock(self.params)
        self.assertEqual(
            self.estado.stock["M4"], self.params["M4"]["CANT_PRODUCCION_DIA"]*2)

    @patch("random.normalvariate")
    def test_genera_pedidos_correctamente(self, mockNormalvariate):
        mockNormalvariate.return_value = 10
        cant_pedidos_generados, cant_total_pedidos = self.estado.generar_pedidos(
            self.params)
        self.assertGreater(len(self.estado.pedidos), 0)
        self.assertEqual(cant_pedidos_generados, 10*3)
        self.assertEqual(cant_total_pedidos, 10*3)

    def test_devuelve_eventos_a_resolver_correctamente(self):
        eventos = [
            PedidoProcesadoEvent(1, 3),     # 4
            PedidoProcesadoEvent(2, 10),    # 12
            PedidoProcesadoEvent(3, 2),     # 5
        ]
        self.estado.eventos = eventos
        eventos_a_resolver = self.estado.get_eventos_a_resolver(5, self.params)
        self.assertEqual(len(eventos_a_resolver), 2)
        self.assertEqual(len(self.estado.eventos), 1)

    def test_cumple_pedidos_correctamente(self):
        self.estado.operarios_disponibles = 2
        self.estado.stock = {
            "M4": 2,
            "M6": 2,
            "M8": 2,
        }
        self.estado.pedidos = [
            Pedido("M4"),
            Pedido("M4"),
            Pedido("M6"),
            Pedido("M8")
        ]
        min = 7

        cant_pedidos_resueltos, cant_pedidos_sin_resolver = self.estado.cumplir_pedidos(
            min, self.params)

        self.assertEqual(len(self.estado.pedidos), 2)
        self.assertEqual(len(self.estado.eventos), 2)
        self.assertEqual(cant_pedidos_resueltos, 2)
        self.assertEqual(cant_pedidos_sin_resolver, 2)

    def test_no_cumple_pedidos_si_falta_stock(self):
        self.estado.operarios_disponibles = 2
        self.estado.stock = {
            "M4": 0,
            "M6": 0,
            "M8": 0,
        }
        self.estado.pedidos = [
            Pedido("M4"),
            Pedido("M4"),
            Pedido("M6"),
            Pedido("M8")
        ]
        min = 7
        cant_pedidos_resueltos, cant_pedidos_sin_resolver = self.estado.cumplir_pedidos(
            min, self.params)

        self.assertEqual(len(self.estado.pedidos), 4)
        self.assertEqual(len(self.estado.eventos), 0)
        self.assertEqual(cant_pedidos_resueltos, 0)
        self.assertEqual(cant_pedidos_sin_resolver, 4)

    def test_no_cumple_pedidos_si_no_hay_operarios_disponibles(self):
        self.estado.operarios_disponibles = 0
        self.estado.stock = {
            "M4": 10,
            "M6": 10,
            "M8": 10,
        }
        self.estado.pedidos = [
            Pedido("M4"),
            Pedido("M4"),
            Pedido("M6"),
            Pedido("M8")
        ]
        min = 7
        self.estado.cumplir_pedidos(min, self.params)

        cant_pedidos_resueltos, cant_pedidos_sin_resolver = self.estado.cumplir_pedidos(
            min, self.params)

        self.assertEqual(len(self.estado.pedidos), 4)
        self.assertEqual(len(self.estado.eventos), 0)
        self.assertEqual(cant_pedidos_resueltos, 0)
        self.assertEqual(cant_pedidos_sin_resolver, 4)
