import unittest
from simulacion.PedidoProcesadoEvent import PedidoProcesadoEvent
from simulacion.Estado import Estado


class TestPedidoProcesadoEvent(unittest.TestCase):

    def setUp(self):
        self.evento = PedidoProcesadoEvent(1, 10)

    def tearDown(self):
        pass

    def test_indica_correctamente_si_se_puede_resolver(self):
        params = {
            "CANT_MINUTOS_JORNADA": 480
        }
        self.assertTrue(self.evento.puede_resolverse(11, params))
        self.assertFalse(self.evento.puede_resolverse(5, params))

    def test_tiempo_inicial_se_resetea_el_evento_termina_despues_de_finalizada_la_jornada(self):
        params = {
            "CANT_MINUTOS_JORNADA": 5
        }

        self.evento.puede_resolverse(3, params)

        self.assertEqual(self.evento.tiempo_inicial, 0)

    def test_evento_se_resuelve_correctamente(self):
        params = {
            "MAX_OPERARIOS": 1
        }
        estado = Estado(params)
        estado.operarios_disponibles -= 1
        self.evento.resolverse(estado, params)
        self.assertEqual(estado.operarios_disponibles,
                         params["MAX_OPERARIOS"])
