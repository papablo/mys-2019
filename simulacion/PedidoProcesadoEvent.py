class PedidoProcesadoEvent:

    def __init__(self, tiempo_inicial: int, demora: int) -> None:
        self.tiempo_inicial = tiempo_inicial
        self.demora = demora

    def puede_resolverse(self, min: int, params: dict) -> bool:
        t = self.tiempo_inicial + self.demora
        cant_minutos_jornada = params.get("CANT_MINUTOS_JORNADA")
        if (t > cant_minutos_jornada):
            self.tiempo_inicial = 0
            return False
        return min >= t

    def resolverse(self, estado: object, params: dict) -> None:
        if estado.operarios_disponibles >= params["MAX_OPERARIOS"]:
            return
        estado.operarios_disponibles += 1
