import numpy as np

from simulacion.PedidoProcesadoEvent import PedidoProcesadoEvent


class Pedido():
    def __init__(self, tipo: str) -> None:
        self.tipo = tipo

    def puede_cumplirse(self, estado: object) -> bool:
        return estado.operarios_disponibles > 0 and estado.stock[self.tipo] > 0

    def cumplirse(self, estado: object, min: int, params: dict) -> PedidoProcesadoEvent:
        estado.stock[self.tipo] -= 1
        estado.operarios_disponibles -= 1
        demora = np.random.exponential(
            params[self.tipo]["MEDIA_ATENCION_PEDIDO"])
        return PedidoProcesadoEvent(min, demora)
