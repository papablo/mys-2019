from simulacion.Pedido import Pedido
from simulacion.PedidoProcesadoEvent import PedidoProcesadoEvent

import random
from typing import Tuple, List


class Estado():
    def __init__(self, params: dict) -> None:
        self.operarios_disponibles = params.get("MAX_OPERARIOS")
        self.pedidos = []
        self.stock = {"M4": 0, "M6": 0, "M8": 0}
        self.eventos = []

    def producir_stock(self, params: dict) -> Tuple[int, ...]:
        stock_producido = []
        for tipo in ["M4", "M6", "M8"]:
            self.stock[tipo] += params.get(tipo).get("CANT_PRODUCCION_DIA")
            stock_producido.append(self.stock[tipo])
        return tuple(stock_producido)

    def generar_pedidos(self, params: dict) -> None:
        cant_pedidos_generados_hoy = 0
        for tipo in ["M4", "M6", "M8"]:
            cant_pedidos_a_generar = int(random.normalvariate(
                params.get(tipo).get("MEDIA_GENERACION_PEDIDOS"),
                params.get(tipo).get("DESVIO_GENERACION_PEDIDOS"),
            ))
            cant_pedidos_generados_hoy += cant_pedidos_a_generar
            pedidos = [Pedido(tipo) for _ in range(cant_pedidos_a_generar)]

            self.pedidos.extend(pedidos)
        return cant_pedidos_generados_hoy, len(self.pedidos)

    def get_eventos_a_resolver(self, min: int, params: dict) -> List[PedidoProcesadoEvent]:
        eventos_a_resolver = []
        eventos_que_no_se_pueden_resolver = []
        for evento in self.eventos:
            if evento.puede_resolverse(min, params):
                eventos_a_resolver.append(evento)
            else:
                eventos_que_no_se_pueden_resolver.append(evento)
        self.eventos = eventos_que_no_se_pueden_resolver
        return eventos_a_resolver

    def cumplir_pedidos(self, min: int, params: dict) -> Tuple[int, int]:
        cant_pedidos_resueltos = 0
        pedidos_que_no_se_pueden_cumplir = []
        for pedido in self.pedidos:
            if pedido.puede_cumplirse(self):
                cant_pedidos_resueltos += 1
                evento = pedido.cumplirse(self, min, params)
                self.eventos.append(evento)
            else:
                pedidos_que_no_se_pueden_cumplir.append(pedido)

        self.pedidos = pedidos_que_no_se_pueden_cumplir
        return cant_pedidos_resueltos, len(pedidos_que_no_se_pueden_cumplir)
