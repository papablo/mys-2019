# MyS-2019

Proyecto Final - Modelos y Simulaciones 2019 - UNPSJB - DIT - Trelew

## Cómo correrlo

- Instalar las dependencias con `pip install -r requirements.txt`
- Activar IPython widgets con `jupyter nbextension enable --py widgetsnbextension` (agregar la bandera `--sys-prefix` si se está trabajando en un virtualenv)
